const users = require('./users.json');

//
// TODO 1
// Obtener el usuario cuyo teléfono es "024-648-3804"
//

console.log('=== 1 ===');
const telephoneUser = users.find(b => b.phone === '024-648-3804');
console.log(telephoneUser);


//
// TODO 2
// Crear una función que devuelva true si existe un usuario cuyo email
// sea el que se pasa como parámetro
//

console.log('=== 2 ===');

const existsUser = email => users.some(b => b.email.includes(email)); // TODO en vez de false, debe devolver el resultado

console.log(existsUser('Nathan@yesenia.net')); // true
console.log(existsUser('japostigo@atsistemas.com')); // false

//
// TODO 3
// Obtener el número de usuarios que tienen website
//

console.log('=== 3 ===');

const numberUserWeb = users.filter(b => b.website);
console.log(numberUserWeb.length);


//
// TODO 4
// Obtener el índice de la posición que toma en el array el primer usuario
// cuyo número de la calle de su dirección es menor que 300
//

console.log('=== 4 ===');

const indexUser = (users.findIndex(b => b.address.number <300));
console.log(indexUser);



//
// TODO 5
// Obtener un array que sólo contenga las cadenas de los emails de los usuarios
//

console.log('=== 5 ===');
const arrayEmail = users.map(b => b.email);
console.log(arrayEmail);



//
// TODO 6
// Obtener un array que contengan objetos {id: "id", username: "username"},
// que contienen los ids y los nombres de usuarios de los usuarios
//

console.log('=== 6 ===');

const arrayid = users.map(b => b.id);
const arrayusername = users.map(b => b.username);

const arrayObjetos = arrayid.map((id, i)=> {
    return{
        'id' : id,
        'username': arrayusername[i]
    }
})

console.log(arrayObjetos);


//
// TODO 7
// Obtener el array de usuarios pero con los números de sus direcciones en
// formato de número (y no de cadena que es como está ahora mismo)
//

console.log('=== 7 ===');
const usersAddressNumber = users.map(b => {
    if (b.address && b.address.number) {
      b.address.number = +b.address.number;
    }
  
    return b;
  });

  console.log(usersAddressNumber);
  


//
// TODO 8
// Obtener el array de usuarios cuya dirección está ubicada entre la
// latitud -50 y 50, y la longitud -100 y 100
//

console.log('=== 8 ===');
const usersLatitudLongitud = users.filter(b => b.address &&
    -50  < +b.address.geo.lat && +b.address.geo.lat < 50  &&
    -100 < +b.address.geo.lng && +b.address.geo.lng < 100);
  console.log(usersLatitudLongitud);


//
// TODO 9
// Obtener un array con los teléfonos de los usuarios cuyo website
// pertenezca a un dominio biz
//

console.log('=== 9 ===');

const phonesWeb = users
  .filter(b => b.website && b.website.endsWith('.biz'))
  .map(b => b.phone);

console.log(phonesWeb);




//
// TODO 10
// Escriba una función processArray que, dado un array de números
// enteros, devuelva un nuevo array en que aquellos elementos que
// sean pares se multipliquen por 2.
//

console.log('=== 10 ===');

const arraynum = [2, 3, 5, 6, 5, 9, 10, 12, 13];

const processArray = b => b.filter(x => x % 2 === 0).map(x => x * 2);

console.log(processArray(arraynum));



//
// TODO 11
// Dado un array de cadenas, obtenga un objeto que tenga como claves cada
// una de las cadenas y cada uno de los valores de esas claves sea false
//

console.log('=== 11 ===');

const keys = ['key1', 'key2', 'key3'];

const obj = keys.reduce((r, k) => ({ ...r, [k]: false }), {});

console.log(obj); // { key1: false, key2: false, key3: false }




//
// TODO 12 (DIFÍCIL)
// Obtenga un objeto de la agrupación de los nombres de usuarios por nombre de
// empresa.
// Las claves del objeto serán los nombres de empresa y los valores de cada
// clave un array con los nombres de pila de los usuarios que pertenecen a esa
// empresa.
//

console.log('=== 12 ===');
const usuariosAgrup = users.reduce((b, x) => ({
    ...b,
    [x.company.name]: [ ...(b[x.company.name] || []), x.name]
  }), {});
  
  console.log(usuariosAgrup);
  

// {
//   'Romaguera-Crona': [ 'Leanne Graham', 'Clementine Bauch' ],
//   'Deckow-Crist': [ 'Ervin Howell', 'Glenna Reichert' ],
//   'Robel-Corkery': [ 'Patricia Lebsack', 'Mrs. Dennis Schulist' ],
//   'Keebler LLC': [ 'Chelsey Dietrich' ],
//   'Hoeger LLC': [ 'Kurtis Weissnat', 'Clementina DuBuque' ],
//   'Abernathy Group': [ 'Nicholas Runolfsdottir V' ]
// }
